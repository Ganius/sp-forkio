Список использованных технологий:
- HTML5
- CSS3
- SCSS
- Gulp
- Node.js



Состав участников проекта:
- Dudenko Margarita
- Telezhenko Pavel



Какие задачи выполнял кто из участников:
    


    Telezhenko Pavel:
        - настройка сборщика Gulp;
        - верстка шапки сайта с верхним меню
        - верстка выпадающего меню при малом разрешении экрана
        - верстка секции People Are Talking About Fork
        
    Dudenko Margarita:
        - верстка блока Revolutionary Editor
        - верстка секции Here is what you get
        - верстка секции Fork Subscription Pricing
